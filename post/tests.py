from django.test import TestCase,Client
from django.urls import resolve
from .views import index
from .models import Message
from .forms import MessageForm

from selenium import webdriver
import time
import unittest

class PostTest(TestCase):

    def test_post_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_confirmation_url_is_exist(self):
        response = Client().get('/confirmation/')
        self.assertEqual(response.status_code,200)

    def test_post_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_post_using_home_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response,'home.html')

    def test_model_create_new_message(self):
        Message.objects.create(name='bambang',message='halo')
        sum = Message.objects.all().count()
        self.assertEqual(sum,1)

    def test_form_validation_for_blank_items(self):
        form = MessageForm(data={'name': '', 'message': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['name'],["This field is required."])
        self.assertEqual(form.errors['message'],["This field is required."])

    def test_form_correct_name(self):
        Message.objects.create(name='bambang')
        message = Message.objects.get(pk=1)
        self.assertEqual(str(message.name), message.name)

    def test_form_correct_message(self):
        Message.objects.create(message='halo')
        message = Message.objects.get(pk=1)
        self.assertEqual(str(message), message.message)

    def test_post_success_and_render_the_result(self):
        response_post = Client().post('/', {'name': 'test', 'message': 'test'})
        self.assertEqual(response_post.status_code, 302)

class FunctionalTest(unittest.TestCase):

    def setUp(self):
        self.driver  = webdriver.Chrome('C:/Users/Lenovo/Downloads/chromedriver_win32/chromedriver.exe')

    def tearDown(self):
        self.driver.quit()

    def test_can_submit_name_message(self):
        self.driver.get('http://localhost:8000')
        time.sleep(5)
        name = self.driver.find_element_by_id('name')
        message = self.driver.find_element_by_id('message')

        time.sleep(5)
        name.send_keys('Steve')
        message.send_keys('Hail Hydra')
        time.sleep(5)
        name.submit()

        confirm_name = self.driver.find_element_by_id('name')
        confirm_name.submit()

        time.sleep(5)
        self.assertIn('Steve', self.driver.page_source)
        self.assertIn('Hail Hydra', self.driver.page_source)

if __name__ == '__main__':
    unittest.main(warnings='ignore')
