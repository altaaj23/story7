from django.shortcuts import render,redirect
from django.http import HttpResponse
from .models import Message
from .forms import MessageForm

form_message = [{}]

def index(request):
    if request.method == 'POST':
        form = MessageForm(request.POST)
        form_message[0]['name'] = request.POST.get('name')
        form_message[0]['message'] = request.POST.get('message')
        if form.is_valid():
            return redirect('/confirmation/')
        else:
            return render(request,'home.html',{'messages':Message.objects.all()})

    return render(request,'home.html',{'messages':Message.objects.all()})

def confirmation(request):
    if request.method == 'POST':
        form = MessageForm(request.POST)
        form.save()
        return redirect ('/')
    else :
        return render(request, 'confirmation.html',{"form_message" : form_message})